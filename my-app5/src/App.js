import React from 'react';
import {Block} from './Block';
import './index.scss';

//криптовалютная страница
function App() {
  //название зависящей валюты
  const [fromCurrency, setFromCurrency] = React.useState('RUB');
  //название зависимой валюты
  const [toCurrency, setToCurrency] = React.useState('USD');
  //состояние количества зависящей валюты 
  const [fromPrice, setFromPrice] = React.useState(0);
  //состояние количества зависимой валюты
  const [toPrice, setToPrice] = React.useState(1);

  //состояние набора стоимостей валют в долларах исходя из их названий
  const ratesRef = React.useRef({});

  //функция изменения количества зависимой валюты по отношению к количеству зависящей валюты
  const onChangeFromPrice = (value) => 
  {
    //считаем количество зависящей валюты в долларах
    const price = value / ratesRef.current[fromCurrency];
    //считаем количество зависимой валюты по исходной валюте
    const result = price * ratesRef.current[toCurrency];
    //фиксируем количество зависимой валюты
    setToPrice(result.toFixed(3));
    //фиксируем количество зависящей валюты
    setFromPrice(value);
  }

  //функция изменения количества зависящей валюты по отношению к количеству зависимой валюты
  const onChangeToPrice = (value) => {
    const result = (ratesRef.current[fromCurrency] / ratesRef.current[toCurrency])*value; 
    //фиксируем количество зависящей валюты по зависимой
    setFromPrice(result.toFixed(3));
    //фиксируем количество зависимой валюты
    setToPrice(value);
  }

  React.useEffect(() => {
    fetch('https://cdn.cur.su/api/latest.json').then((res) => res.json()).then((json) => 
    {
      //получили набор стоимостей валют по разным валютам исходя из доллара
      ratesRef.current = json.rates;
      onChangeToPrice(1);
    })
    .catch((err) => {
      console.warn(err);
      alert('Failed to get information')
    })
  }, [onChangeToPrice])

  React.useEffect(() => {
    //меняем количество зависящей валюты по зависимой в зависимости от изменения названия зависящей валюты
    onChangeFromPrice(fromPrice);
  }, [onChangeFromPrice, fromPrice, fromCurrency]);

  React.useEffect(() => {
    //меняем количество зависимой валюты по зависящей в зависимости от изменения названия зависимой валюты
    onChangeToPrice(toPrice);
  }, [onChangeToPrice, toPrice, toCurrency]);

  return (
    <div className="App">
      {/*показываем блок зависящих валют */}
      <Block 
        //количество зависящей валюты
        value={fromPrice} 
        //название зависящей валюты 
        currency={fromCurrency} 
        //функция изменения названия зависящей валюты
        onChangeCurrency={setFromCurrency} 
        //функция изменения количества зависимой валюты по отношению к количеству зависящей валюты
        onChangeValue={onChangeFromPrice}
      />
      {/*показываем блок зависимых валют */}
      <Block 
        //количество зависимой валюты
        value={toPrice}
        //название зависимой валюты 
        currency={toCurrency} 
        //функция изменения названия зависимой валюты
        onChangeCurrency={setToCurrency}
        //функция изменения количества зависящей валюты по отношению к количеству зависимой валюты
        onChangeValue={onChangeToPrice}
      />   
    </div>
  );
}

export default App;