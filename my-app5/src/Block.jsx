import React from 'react';

const defaultCurrencies = ['RUB', 'USD', 'EUR', 'GBP'];

//блок валют зависящих или зависимых
export const Block = ({
    //количество зависящей или зависимой валюты
    value, 
    //название зависящей или зависимой валюты 
    currency, 
    //функция изменения названия зависящей или зависимой валюты
    onChangeValue, 
    //функция изменения количества зависимой валюты по отношению к количеству зависящей валюты или
    //функция изменения количества зависящей валюты по отношению к количеству зависимой валюты
    onChangeCurrency
}) => (
    <div className="block">
        <ul className="currencies">
            {defaultCurrencies.map((cur) => (
                //предоставляем возможность изменения названия зависящей или зависимой валюты
                <li
                    onClick={() => onChangeCurrency(cur)}
                    className={currency === cur ? 'active' : ''}
                    key={cur}
                >
                    {cur}
                </li>
            ))}
            <li>
                <svg height="50px" viewBox="0 0 50 50" width="50px">
                    <rect fill="none" height="50" width="50"/>
                    <polygon points="47.25,15 45.164,12.914 25,33.078 4.836,12.914 2.75,15 25,37.25 " />
                </svg>
            </li>
        </ul>
        {/*предоставляем возможность изменения количества зависящей или зависимой валюты*/}
        <input
            onChange={(e) => onChangeValue(e.target.value)}
            value={value}
            type="number"
            placeholder={0}
        />
    </div>
)