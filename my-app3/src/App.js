import React from 'react';
import './index.scss';

//структура вопросов, вариантов ответа на них и правильный ответ среди этих вариантов
const questions = [
  {
    //первый вопрос
    title: 'React - this ... ?',
    //варианты ответа на первый вопрос 
    variants: ['library', 'framework', 'application'],
    //правильный ответ на первый вопрос
    correct: 0,
  },
  {
    //второй вопрос
    title: 'Component - this ... ',
    //варианты ответа на второй вопрос 
    variants: ['application', 'part of an application or page', 'What I do not know what is'],
    //правильный ответ на второй вопрос
    correct: 1,
  },
  {
    //третий вопрос
    title: 'What is it JSX?',
    //варианты ответа на третий вопрос 
    variants: [
      'It is simple HTML',
      'It is function',
      'It is same Это тот же HTML, but with possibility to implement JS-code',
    ],
    //правильный ответ на третий вопрос
    correct: 2,
  }
]

//функция показа результата игры
function Result({correct}) {
  return (
    <div className="result">
      <img src="https://cdn-icons-png.flaticon.com/512/2278/2278992.png" />
      {/*Отображение количества правильных ответов */}
      <h2>
        You guessed it {correct} answer from {questions.length}
      </h2>
       {/*Предоставление возможности перезапустить игру */}
      <a href="/">
        <button>Try again</button>
      </a>
    </div>
  )
}

//функция самой игры
function Game({step, question, onClickVariant}) 
{
  //определяем в процентном соотношении количество вопросов игры, на которых уже получен ответ
  const percentage = Math.round((step / questions.length) * 100);
  return (
    <>
      {/*показываем в процентном соотношении количество вопросов игры, на которых уже получен ответ */}
      <div className="progress">
        <div style={{width: `${percentage}%`}} className="progress__inner"></div>
      </div>
      {/*вопрос игры*/}
      <h1>{question.title}</h1>
      <ul>
        {
          //варианты ответов на вопрос игры, на некоторый из них нужно нажать 
          question.variants.map((text, index) => (
          <li key={text} onClick={() => onClickVariant(index)}>
            {text}
          </li>
        ))}
      </ul>
    </>
  )
}


function App() {
  //состояние номера этапа игры
  const [step, setStep]       = React.useState(0);
  //состояние числа правильных ответов на вопрос игры
  const [correct, setCorrect] = React.useState(0);
  //вопрос игры по этапу step
  const question = questions[step];

  //функция управления процессом игры (правильный ли ответ дан на вопрос игры или нет)
  const onClickVariant = (index) => {
    //фиксируем номер этапа игры
    setStep(step+1);

    if (index === question.correct) 
    {
      //фиксируем число правильных ответов на вопросы игры
      setCorrect(correct + 1);
    }
  }

  return (
    <div className="App">
      { step !== questions.length ?
      (
        //Показываем этапы игры, если нет ответов на все вопросы
        <Game
          //номер этапа игры
          step={step}  
          //вопрос игры по соответствующему ее этапу
          question={question} 
          //функция проведения самой игры
          onClickVariant={onClickVariant}
        />
      )
      : 
      (
        //Показываем результат самой игры
        <Result correct = {correct}/> 
      )}
    </div>
  );
}

export default App;