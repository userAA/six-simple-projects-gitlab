gitlab page https://gitlab.com/userAA/six-simple-projects-gitlab.git
gitlab six-simple-projects-gitlab

a. проект my-app1 (счетчик).
используемые технологии
    @testing-library/jest-dom,
    @testing-library/react,
    @testing-library/user-event,
    react,
    react-dom,
    react-scripts,
    sass.

b. проект my-app2 (модальное окно).
используемые технологии
    @testing-library/jest-dom,
    @testing-library/react,
    @testing-library/user-event,
    react,
    react-dom,
    react-scripts,
    sass.

c. проект my-app3 (опросник).
используемые технологии
    @testing-library/jest-dom,
    @testing-library/react,
    @testing-library/user-event,
    react,
    react-dom,
    react-scripts,
    sass.

d. project my-app4 (список пользователей).
используемые технологии
    @testing-library/jest-dom,
    @testing-library/react,
    @testing-library/user-event,
    react,
    react-content-loader,
    react-dom,
    react-scripts,
    sass.

e. project my-app5 (конвертер валют).
используемые технологии
    @emotion/react,
    @emotion/styled,
    @mui/icons-material,
    @mui/material,
    @testing-library/jest-dom,
    @testing-library/react,
    @testing-library/user-event,
    axios,
    framer-motion,
    react,
    react-dom,
    react-scripts,
    sass.

f. project my-app6 (коллекция фотографий).
используемые технологии
    @testing-library/jest-dom,
    @testing-library/react,
    @testing-library/user-event,
    react,
    react-dom,
    react-scripts,
    sass.