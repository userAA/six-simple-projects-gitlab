import React from 'react';
import {Skeleton} from './Skeleton';
import {User} from './User';

export const Users = ({
    //информация о пользователях
    items, 
    //флаг скачивания информации о пользователях
    isLoading, 
    //метка поиска пользователя из имеющегося списка пользователей по его полному имени
    searchValue, 
    //функция изменения метки поиска пользователя из имеющегося списка пользователей по его полному имени
    onChangeSearchValue,
    //массив идентификаторов пользователей, которым отправлены приглашения
    invites,
    //функция отправления приглашения конкретному пользователю
    onClickInvite,
    //функция фиксации отправки приглашений пользователям
    onClickSendInvites
}) => {
    return (
        <>
            <div className="search">
                {/*Значок поиска пользователя */}
                <svg viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg">
                    <path d="M12.9 14.32a8 8 0 1 1 1.41-1.41l5.35 5.33-1.42 1.42-5.33-5.34zM8 14A6 6 0 1 0 8 2a6 6 0 0 0 0 12z" />
                </svg>
                {/*Окно задания метки пользователя  */}
                <input 
                    //значение метки поиска пользователя из имеющегося списка пользователей по его полному имени
                    value={searchValue} 
                    //изменение значения метки поиска пользователя из имеющегося списка пользователей по его полному имени
                    onChange={onChangeSearchValue} 
                    type="text" 
                    placeholder="Find a user..."
                />
            </div>
            {isLoading ? (
                //информация о пользователях грузится
                <div className="skeleton-list">
                    <Skeleton/>
                    <Skeleton/>
                    <Skeleton/>
                </div>
                ) : (
                <ul className="users-list">
                    {/*Выводим информацию о каждом пользователе с учетом метки 
                       фильтрации пользователей searchValue по их полным именам*/}
                    {items.filter(obj => {
                        const fullName = (obj.first_name + obj.last_name).toLowerCase();
                        return (
                            fullName.includes(searchValue.toLowerCase()) || 
                            obj.email.toLowerCase().includes(searchValue.toLowerCase())
                        );
                    })
                    .map((obj) => (
                        //выводим каждого пользователя
                        <User 
                            //функция отправления приглашения отдельному пользователю
                            onClickInvite={onClickInvite}
                            //флаг, говорящий о том что отдельному пользователю отправлено приглашение
                            isInvited={invites.includes(obj.id)} 
                            key={obj.id} 
                            {...obj} 
                        />
                    ))}
                </ul>
            )}
            {/*Переход на страницу показа количества приглашенных пользователей если они есть */}
            {invites.length > 0 && (
                <button onClick={onClickSendInvites} className="send-invite-btn">
                    Send invitation
                </button>
            )}
        </>
    )
}