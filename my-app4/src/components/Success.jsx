import React from 'react';

//показываем число пользователей, которым отправлено приглашение
export const Success = ({
    //число пользователей, которым отправлено приглашение
    count 
}) => {
    return (
        <div class="success-block">
            <img src="/assets/success.svg" alt="Success" />
            <h3>Successfull!</h3>
            <p>All {count} users the invitation was sent.</p>
            {/*Кнопка перехода на исходную страницу проекта */}
            <button onClick={() => window.location.reload()} className="send-invite-btn">Back</button>
        </div>
    )
}