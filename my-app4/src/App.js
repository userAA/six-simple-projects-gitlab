import React from 'react';
import './index.scss';
import {Success} from './components/Success';
import {Users} from './components/Users/index';

// Тут массив пользователей: https://reqres.in/api/users

function App() 
{
  //информация о пользователях
  const [users, setUsers]             = React.useState([]);
  //массив идентификаторов пользователей, которым отправлены приглашения
  const [invites, setInvites]         = React.useState([]);
  //флаг скачивания информации о пользователях с сайта https://reqres.in/api/users
  const [isLoading, setLoading]       = React.useState(true);
  //флаг успеха отправления любого количества приглашений пользователям
  const [success, setSuccess]         = React.useState(false);
  //состояние метки поиска пользователя по его полному имени
  const [searchValue, setSearchValue] = React.useState('');

  React.useEffect(() => 
  {
    fetch('https://reqres.in/api/users').then(res => res.json()).then(json => 
    {
      //пользователи получены
      setUsers(json.data);
    })
    .catch(err => 
    {
      console.warn(err);
      alert('Error receiving users');
    })
    .finally(() => setLoading(false));
  }, [])

  //функция изменения метки поиска пользователя по его полному имени
  const onChangeSearchValue = (event) => 
  {
    setSearchValue(event.target.value);
  }

  //функция контроля приглашений пользователям
  const onClickInvite = (id) => 
  {
    if (invites.includes(id)) 
    {
      //пользователь с идентификатором id уже имеет приглашение, значит с него это приглашение снимается
      setInvites( (prev) => prev.filter( (_id) => _id !== id))
    } 
    else 
    {
      //в противном случае пользователю с идентификатором id предоставляем приглашение
      setInvites( (prev) => [...prev, id]);
    }
  }

  //функция фиксации успеха по отправлениям приглашений пользователям 
  const onClickSendInvites = () => 
  {
    setSuccess(true);
  }

  return (
    <div className="App">
      {success ? 
        (
          //фиксируем успех отправлений приглашений пользователям
          <Success count={invites.length} /> 
        ) 
        : 
        (
          //выводим пользователей
          <Users 
            //информация о пользователях
            items={users} 
            //флаг скачивания информации о пользователях 
            isLoading={isLoading}
            //метка поиска пользователя из имеющегося списка пользователей по его полному имени
            searchValue={searchValue}           
            //функция изменения метки поиска пользователя из имеющегося списка пользователей по его полному имени
            onChangeSearchValue={onChangeSearchValue}
            //массив идентификаторов пользователей, которым отправлены приглашения
            invites={invites}
            //функция отправления приглашения конкретному пользователю
            onClickInvite={onClickInvite}
            //функция фиксации отправки приглашений пользователям
            onClickSendInvites={onClickSendInvites}
          />
        )
      }
    </div>
  );
}

export default App;